<?php
session_start();
require '../config/bootstrap.php';
require '../config/requirements.php';

/**
 * Load the bootstrap file.
 */

?>
<?php include 'inc/header.php'; ?>
<?php include 'inc/nav.php'; ?>

<?php
require_once '../config/connect.php';
if (!isset($_SESSION['email']) & empty($_SESSION['email'])) {
    header('location: login.php');
}

if (isset($_POST) && !empty($_POST)) {
    $prodname = mysqli_real_escape_string($connection, $_POST['productname']);
    $description = mysqli_real_escape_string($connection, $_POST['productdescription']);
    $category = mysqli_real_escape_string($connection, $_POST['productcategory']);
    $price = mysqli_real_escape_string($connection, $_POST['productprice']);


    if (isset($_FILES) & !empty($_FILES)) {
        $name = $_FILES['productimage']['name'];
        $size = $_FILES['productimage']['size'];
        $type = $_FILES['productimage']['type'];
        $tmp_name = $_FILES['productimage']['tmp_name'];

        $max_size = 10000000;
        $extension = substr($name, strpos($name, '.') + 1);

        if (isset($name) && !empty($name)) {
            if (($extension == "jpg" || $extension == "jpeg") && $type == "image/jpeg") {
                $location = "uploads/";
                if (move_uploaded_file($tmp_name, $location.$name)) {
                    //$smsg = "Uploaded Successfully";
                    $sql = "INSERT INTO products (name, description, catid, price, thumb) VALUES ('$prodname', '$description', '$category', '$price', '$location$name')";
                    $res = mysqli_query($connection, $sql);
                    if ($res) {
                        //echo "Product Created";
                        header('location: products.php');
                    } else {
                        $fmsg = "Error al crear producto";
                    }
                } else {
                    $fmsg = "Error al cargar el archivo";
                }
            } else {
                $fmsg = "Solo se permiten archivos JPG y deben tener menos de 1 MB";
            }
        } else {
            $fmsg = "Por favor seleccione un archivo";
        }
    } else {
        $sql = "INSERT INTO products (name, description, catid, price) VALUES ('$prodname', '$description', '$category', '$price')";
        $res = mysqli_query($connection, $sql);
        if ($res) {
            header('location: products.php');
        } else {
            $fmsg =  "Error al crear producto";
        }
    }
}
?>


<section id="content">
    <div class="content-blog">
        <div class="container">
            <?php if (isset($fmsg)) {
    ?><div class="alert alert-danger" role="alert"> <?php echo $fmsg; ?> </div><?php
} ?>
            <?php if (isset($smsg)) {
        ?><div class="alert alert-success" role="alert"> <?php echo $smsg; ?> </div><?php
    } ?>
            <form method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="Productname">Nombre del producto</label>
                    <input type="text" class="form-control" name="productname" id="Productname" placeholder="Nombre del producto">
                </div>
                <div class="form-group">
                    <label for="productdescription">Descripción</label>
                    <textarea class="form-control" name="productdescription" rows="3"></textarea>
                </div>

                <div class="form-group">
                    <label for="productcategory">Categoría</label>
                    <select class="form-control" id="productcategory" name="productcategory">
                        <option value="">---Seleccione una categoría---</option>
                        <?php
                        $sql = "SELECT * FROM category";
                        $res = mysqli_query($connection, $sql);
                        while ($r = mysqli_fetch_assoc($res)) {
                            ?>
                            <option value="<?php echo $r['id']; ?>"><?php echo $r['name']; ?></option>
                        <?php
                        } ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="productprice">Precio del producto</label>
                    <input type="text" class="form-control" name="productprice" id="productprice" placeholder="Product Price">
                </div>
                <div class="form-group">
                    <label for="productimage">Imagen del producto</label>
                    <input type="file" name="productimage" id="productimage">
                    <p class="help-block">Solo se permiten jpg / png.</p>
                </div>

                <button type="submit" class="btn btn-default">Enviar</button>
            </form>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
        </div>
    </div>
</section>

<?php include 'inc/footer.php'; ?>

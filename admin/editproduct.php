<?php
session_start();
require '../config/bootstrap.php';
require_once '../config/connect.php';
if (!isset($_SESSION['email']) & empty($_SESSION['email'])) {
    header('location: login.php');
}

if (isset($_GET) & !empty($_GET)) {
    $id = $_GET['id'];
} else {
    header('location: products.php');
}

if (isset($_POST) && !empty($_POST)) {
    $prodname = mysqli_real_escape_string($connection, $_POST['productname']);
    $description = mysqli_real_escape_string($connection, $_POST['productdescription']);
    $category = mysqli_real_escape_string($connection, $_POST['productcategory']);
    $price = mysqli_real_escape_string($connection, $_POST['productprice']);

    if (isset($_FILES) & !empty($_FILES)) {
        $name = $_FILES['productimage']['name'];
        $size = $_FILES['productimage']['size'];
        $type = $_FILES['productimage']['type'];
        $tmp_name = $_FILES['productimage']['tmp_name'];

        $max_size = 10000000;
        $extension = substr($name, strpos($name, '.') + 1);

        if (isset($name) && !empty($name)) {
            if (($extension == "jpg" || $extension == "jpeg") && $type == "image/jpeg" && $size<=$max_size) {
                $location = "uploads/";
                $filepath = $location.$name;
                if (move_uploaded_file($tmp_name, $filepath)) {
                    $smsg = "Subido correctamente";
                } else {
                    $fmsg = "Error al cargar el archivo";
                }
            } else {
                $fmsg = "Solo se permiten archivos JPG y deben tener menos de 1 MB";
            }
        } else {
            $fmsg = "Por favor seleccione un archivo";
        }
    } else {
        $filepath = $_POST['filepath'];
    }

    $sql = "UPDATE products SET name='$prodname', description='$description', catid='$category', price='$price', thumb='$filepath' WHERE id = $id";
    $res = mysqli_query($connection, $sql);
    if ($res) {
        $smsg = "Producto actualizado";
    } else {
        $fmsg = "Error al actualizar el producto";
    }
}
?>
<?php include 'inc/header.php'; ?>
<?php include 'inc/nav.php'; ?>

<section id="content">
    <div class="content-blog">
        <div class="container">
            <?php if (isset($fmsg)) { ?>
                <div class="alert alert-danger" role="alert"><?php echo $fmsg; ?></div>
            <?php } ?>
            <?php if (isset($smsg)) { ?>
                <div class="alert alert-success" role="alert"><?php echo $smsg; ?></div>
            <?php } ?>
            <?php
            $sql = "SELECT * FROM products WHERE id=$id";
            $res = mysqli_query($connection, $sql);
            $r = mysqli_fetch_assoc($res);
            ?>
            <form method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <input type="hidden" name="filepath" value="<?php echo $r['thumb']; ?>">
                    <label for="Productname">Nombre del producto</label>
                    <input type="text" class="form-control" name="productname" id="Productname" placeholder="Nombre del producto" value="<?php echo $r['name']; ?>">
                </div>
                <div class="form-group">
                    <label for="productdescription">Descripción del producto</label>
                    <textarea class="form-control" name="productdescription" rows="3"><?php echo $r['description']; ?></textarea>
                </div>

                <div class="form-group">
                    <label for="productcategory">Categoria de producto</label>
                    <select class="form-control" id="productcategory" name="productcategory">
                        <?php
                        $catsql = "SELECT * FROM category";
                        $catres = mysqli_query($connection, $catsql);
                        while ($catr = mysqli_fetch_assoc($catres)) {
                            ?>
                            <option value="<?php echo $catr['id']; ?>" <?php if ($catr['id'] == $r['catid']) {
                                echo "selected";
                            } ?>><?php echo $catr['name']; ?></option>
                            <?php
                        } ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="productprice">Precio del producto</label>
                    <input type="text" class="form-control" name="productprice" id="productprice" placeholder="Precio del producto" value="<?php echo $r['price']; ?>">
                </div>

                <div class="form-group">
                    <label for="productimage">Imagen del producto</label>
                    <?php if (isset($r['thumb']) & !empty($r['thumb'])) { ?>
                        <br>
                        <img src="<?php echo $r['thumb'] ?>" widht="100px" height="100px">
                        <a href="delprodimg.php?id=<?php echo $r['id']; ?>">Eliminar Imagen</a>
                        <?php
                    } else { ?>
                        <input type="file" name="productimage" id="productimage">
                        <p class="help-block">Solo se permiten jpg / png.</p>
                    <?php } ?>
                </div>
                <button type="submit" class="btn btn-default">Enviar</button>
            </form>
            <br>
            <br>
            <br><br>
        </div>
    </div>
</section>

<?php include 'inc/footer.php'; ?>

        <div class="menu-wrap">
            <div id="mobnav-btn">Menu <i class="fa fa-bars"></i></div>
            <ul class="sf-menu">
                <li>
                    <a href="index.php">Tablero</a>
                </li>
                <li>
                    <a href="#">Categorías</a>
                    <div class="mobnav-subarrow"><i class="fa fa-plus"></i></div>
                    <ul>
                        <li><a href="categories.php">Ver categorias</a></li>
                        <li><a href="addcategory.php">Agregar categoría</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">Productos</a>
                    <div class="mobnav-subarrow"><i class="fa fa-plus"></i></div>
                    <ul>
                        <li><a href="products.php">Ver productos</a></li>
                        <li><a href="addproduct.php">Agregar productos</a></li>
                    </ul>
                </li>

                <li>
                    <a href="#">Pedidos</a>
                    <div class="mobnav-subarrow"><i class="fa fa-plus"></i></div>
                    <ul>
                        <li><a href="orders.php">Ver pedidos</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">Clientes</a>
                    <div class="mobnav-subarrow"><i class="fa fa-plus"></i></div>
                    <ul>
                        <li><a href="customers.php">Ver clientes</a></li>
                        <li><a href="reviews.php">Ver comentarios</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">Mi cuenta</a>
                    <div class="mobnav-subarrow"><i class="fa fa-plus"></i></div>
                    <ul>
                        <li><a href="">Editar Perfil</a></li>
                        <li><a href="logout.php">Cerrar Sesión</a></li>
                    </ul>
                </li>
            </ul>
            <div class="header-xtra">
                <div class="s-cart">
                    <div class="sc-ico"><i class="fa fa-shopping-cart"></i><em>2</em></div>

                    <div class="cart-info">
                        <small>Usted tiene <em class="highlight">2 item(s)</em> bolsa de compras</small>
                        <br>
                        <br>
                        <div class="ci-item">
                            <img src="images/shop/2.jpg" width="70" alt=""/>
                            <div class="ci-item-info">
                                <h5><a href="./single-product.html">Product fashion</a></h5>
                                <p>2 x $250.00</p>
                                <div class="ci-edit">
                                    <a href="#" class="edit fa fa-edit"></a>
                                    <a href="#" class="edit fa fa-trash"></a>
                                </div>
                            </div>
                        </div>
                        <div class="ci-item">
                            <img src="images/shop/8.jpg" width="70" alt=""/>
                            <div class="ci-item-info">
                                <h5><a href="./single-product.html">Product fashion</a></h5>
                                <p>2 x $250.00</p>
                                <div class="ci-edit">
                                    <a href="#" class="edit fa fa-edit"></a>
                                    <a href="#" class="edit fa fa-trash"></a>
                                </div>
                            </div>
                        </div>
                        <div class="ci-total">Subtotal: $750.00</div>
                        <div class="cart-btn">
                            <a href="#">View Bag</a>
                            <a href="#">Checkout</a>
                        </div>
                    </div>
                </div>
                <div class="s-search">
                    <div class="ss-ico"><i class="fa fa-search"></i></div>
                    <div class="search-block">
                        <div class="ssc-inner">
                            <form method="post" action="index.php">
                                <input type="text" placeholder="Buscar producto">
                                <button type="submit"><i class="fa fa-search"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

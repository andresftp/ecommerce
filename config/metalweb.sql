-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 29-08-2019 a las 15:52:27
-- Versión del servidor: 5.7.27-0ubuntu0.19.04.1
-- Versión de PHP: 7.2.21-1+ubuntu19.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `metalweb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `admin`
--

INSERT INTO `admin` (`id`, `firstname`, `lastname`, `email`, `password`) VALUES
(1, 'Justin', 'Hartman', 'admin@admin.com', '202cb962ac59075b964b07152d234b70');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'Ventanas'),
(2, 'Rejas'),
(3, 'Puertas'),
(4, 'Portones'),
(5, 'Mas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orderitems`
--

CREATE TABLE `orderitems` (
  `id` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `orderid` int(11) NOT NULL,
  `pquantity` varchar(255) NOT NULL,
  `productprice` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `orderitems`
--

INSERT INTO `orderitems` (`id`, `pid`, `orderid`, `pquantity`, `productprice`) VALUES
(1, 1, 1, '1', '750000'),
(2, 43, 1, '1', '1300000');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `totalprice` varchar(255) NOT NULL,
  `orderstatus` varchar(255) NOT NULL,
  `paymentmode` varchar(255) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `orders`
--

INSERT INTO `orders` (`id`, `uid`, `totalprice`, `orderstatus`, `paymentmode`, `timestamp`) VALUES
(1, 6, '2050000', 'Despachado', 'cod', '2019-08-29 14:13:14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ordertracking`
--

CREATE TABLE `ordertracking` (
  `id` int(11) NOT NULL,
  `orderid` int(11) NOT NULL,
  `status` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ordertracking`
--

INSERT INTO `ordertracking` (`id`, `orderid`, `status`, `message`, `timestamp`) VALUES
(1, 1, 'Dispatched', ' Se fue', '2019-08-29 14:38:19'),
(2, 1, 'In Progress', ' En progreso', '2019-08-29 14:41:29'),
(3, 1, 'Despachado', ' ', '2019-08-29 14:41:40');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `catid` int(11) NOT NULL,
  `price` varchar(255) NOT NULL,
  `thumb` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `products`
--

INSERT INTO `products` (`id`, `name`, `catid`, `price`, `thumb`, `description`) VALUES
(1, 'Escalera', 5, '750000', 'uploads/mas/mas1.jpg', 'Escalera metalica color   negra'),
(2, 'Escalera', 5, '500000', 'uploads/mas/mas2.jpg', 'Escalera metalica color   negra'),
(3, 'Escalera', 5, '250000', 'uploads/mas/mas3.jpg', 'Escalera metalica color   negra'),
(4, 'Escalera', 5, '180000', 'uploads/mas/mas4.jpg', 'Escalera metalica color   negra'),
(5, 'Escalera', 5, '200000', 'uploads/mas/mas5.jpg', 'Escalera metalica color   negra'),
(6, 'Techo', 5, '300000', 'uploads/mas/mas 01.jpg', 'Techo metalico color '),
(7, 'Techo', 5, '430000', 'uploads/mas/mas 02.jpg', 'Techo metalico color '),
(8, 'Techo', 5, '280000', 'uploads/mas/mas 03.jpg', 'Techo metalico color '),
(9, 'Techo', 5, '850000', 'uploads/mas/mas 04.jpg', 'Techo metalico color '),
(10, 'Techo', 5, '900000', 'uploads/mas/mas 05.jpg', 'Techo metalico color '),
(11, 'puerta', 3, '8000', 'uploads/puertas/puerta 1.jpg', 'puerta metalica color café con chapa y pasador de seguridad con unas medidas de  201cm de alto x 82 cm de ancho'),
(12, 'puerta', 3, '900000', 'uploads/puertas/puerta 2.jpg', 'puerta metalica color café sin ventana con pasador y chapa de seguridad con medidas de  62,5 cm,de alto 72,5 cm, de ancho'),
(13, 'puerta', 3, '260000', 'uploads/puertas/puerta 3.jpg', 'puertametalica color gris de chapa y pasador de seguridad con medidas  de  62,5 cm,de alto 72,5 cm, de ancho'),
(14, 'puerta', 3, '110000', 'uploads/puertas/puerta 4.jpg', 'purta color blanca con ventana grande chapa y pasador de seguridad con medidas  de  201cm de alto x 82 cm de ancho'),
(15, 'puerta', 3, '300000', 'uploads/puertas/puerta 5.jpg', 'puerta color blanca en metal con ventanillas diagona con chapa y pasador de seguridad con medidas 62,5 cm,de alto 72,5 cm, de ancho'),
(16, 'puerta', 3, '500000', 'uploads/puertas/puerta 6.jpg', 'puerta doble blanca en metal con chapa y pasador de seguridad con medidas 3,5m de ancho por 2,5 de alto'),
(17, 'puerta', 3, '630', 'uploads/puertas/puerta 7.jpg', 'puerta metalica gris con ventanal a lado larga con chapa y pasador de seguridad con medidas de 62,5 de ancho x 203 de alto'),
(18, 'puerta', 3, '280000', 'uploads/puertas/puerta 8.jpg', 'puerta metalica negra con ventana grande chapa y pasador de seguridad con medidas 62,5 cm,de alto 72,5 cm, de ancho'),
(19, 'puerta', 3, '900000', 'uploads/puertas/puerta 9.jpg', 'puerta blanca con bentanilla horisontal con chapa de seguridad 62,5 cm,de alto 72,5 cm, de ancho'),
(20, 'Portones', 4, '750000', 'uploads/porton/porton 1.jpg', 'Porton metalico color  Negro con una medida de 160 x 170'),
(21, 'Portones', 4, '500000', 'uploads/porton/porton 2.jpg', 'Porton metalico color   Café con una medidas de 160 x 170'),
(22, 'Portones', 4, '250000', 'uploads/porton/porton 3.jpg', 'Porton metalico color   Negro con una medidas de 150 x 160'),
(23, 'Portones', 4, '180000', 'uploads/porton/porton 4.jpg', 'Porton metalico color  Café  con una medidas de 120 x 150'),
(24, 'Portones', 4, '200000', 'uploads/porton/porton 5.jpg', 'Porton metalico color  Negro con una medidas de 160 x 170'),
(25, 'Portones', 4, '300000', 'uploads/porton/porton 6.jpg', 'Porton metalico color  Blanco con una medidas de 120 x150'),
(26, 'Portones', 4, '430000', 'uploads/porton/porton 7.jpg', 'Porton metalico color  Blanco con una medidas de 110 x 180'),
(27, 'Portones', 4, '280000', 'uploads/porton/porton 8.jpg', 'Porton metalico color Negro con una medidas de 160 x 170'),
(28, 'Portones', 4, '850000', 'uploads/porton/porton 9.jpg', 'Porton metalico color Café con una medidas de 160 x 170'),
(29, 'Portones', 4, '900000', 'uploads/porton/porton 10.jpg', 'Porton metalico color Café con una medidas de 160 x 170'),
(30, 'ventanas', 1, '300000', 'uploads/ventanas/ventana 1.jpg', 'ventana color negra medidas 80*140'),
(31, 'ventanas', 1, '500000', 'uploads/ventanas/ventana 2.jpg', 'ventana color negra medidas 150*170'),
(32, 'ventanas', 1, '600000', 'uploads/ventanas/ventana 3.jpg', 'ventana color negra medidas 130*140'),
(33, 'ventanas', 1, '200000', 'uploads/ventanas/ventana 4.jpg', 'ventana color negra medidas 130*110'),
(34, 'ventanas', 1, '350000', 'uploads/ventanas/ventana 5.jpg', 'ventana color negra medidas 160*140'),
(35, 'ventanas', 1, '500000', 'uploads/ventanas/ventana 6.jpg', 'ventana color blanca medidas 180*160'),
(36, 'ventanas', 1, '450000', 'uploads/ventanas/ventana 8.jpg', 'ventana color gris medidas 170*150'),
(37, 'ventanas', 1, '300000', 'uploads/ventanas/ventana 11.jpg', 'ventano color negra meidas 150*120'),
(38, 'ventanas', 1, '500000', 'uploads/ventanas/ventana 12.jpg', 'ventana color negra medidas 130*150'),
(39, 'ventanas', 1, '450000', 'uploads/ventanas/ventanas 13.jpg', 'ventana color negra medidas 140*130'),
(40, 'rejas', 2, '800000', 'uploads/rejas/reja 2.jpg', 'reja color café '),
(41, 'rejas', 2, '2200000', 'uploads/rejas/reja 3.jpg', 'reja color blanca'),
(42, 'rejas', 2, '300000', 'uploads/rejas/rejas 4.jpg', 'rejas para balcon color blanca'),
(43, 'rejas', 2, '1300000', 'uploads/rejas/reja 5.jpg', 'reja color negra'),
(44, 'rejas', 2, '3800000', 'uploads/rejas/reja 11.jpg', 'reja color negra'),
(45, 'rejas', 2, '1300000', 'uploads/rejas/reja 12.jpg', 'reja color verde');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reviews`
--

CREATE TABLE `reviews` (
  `id` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `review` text NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `reviews`
--

INSERT INTO `reviews` (`id`, `pid`, `uid`, `review`, `timestamp`) VALUES
(1, 31, 6, 'Excelente producto', '2019-08-29 14:05:35');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `timestamp`) VALUES
(6, 'andresftps@gmail.com', '202cb962ac59075b964b07152d234b70', '2019-08-28 22:27:21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usersmeta`
--

CREATE TABLE `usersmeta` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `company` varchar(255) NOT NULL,
  `address1` varchar(255) NOT NULL,
  `address2` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `zip` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usersmeta`
--

INSERT INTO `usersmeta` (`id`, `uid`, `firstname`, `lastname`, `company`, `address1`, `address2`, `city`, `state`, `country`, `zip`, `mobile`) VALUES
(3, 6, 'An', 'T', 'TNT', 'Calle 123', '', 'Bogota', 'Huila', 'Colombia', '10014', '123456');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wishlist`
--

CREATE TABLE `wishlist` (
  `id` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `wishlist`
--

INSERT INTO `wishlist` (`id`, `pid`, `uid`, `timestamp`) VALUES
(1, 6, 6, '2019-08-29 13:59:21');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email_idx` (`email`);

--
-- Indices de la tabla `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `orderitems`
--
ALTER TABLE `orderitems`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_pid_products_id_idx` (`pid`),
  ADD KEY `fk_orderid_orders_id_idx` (`orderid`);

--
-- Indices de la tabla `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_uid_users_id_idx` (`uid`);

--
-- Indices de la tabla `ordertracking`
--
ALTER TABLE `ordertracking`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_orderid_orders_id_idx` (`orderid`);

--
-- Indices de la tabla `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_catid_category_id_idx` (`catid`);

--
-- Indices de la tabla `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_pid_products_id_idx` (`pid`),
  ADD KEY `fk_uid_users_id_idx` (`uid`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email_idx` (`email`);

--
-- Indices de la tabla `usersmeta`
--
ALTER TABLE `usersmeta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_uid_users_id_idx` (`uid`);

--
-- Indices de la tabla `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_uid_users_id_idx` (`uid`),
  ADD KEY `fk_pid_products_id_idx` (`pid`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `orderitems`
--
ALTER TABLE `orderitems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `ordertracking`
--
ALTER TABLE `ordertracking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT de la tabla `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `usersmeta`
--
ALTER TABLE `usersmeta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `wishlist`
--
ALTER TABLE `wishlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `orderitems`
--
ALTER TABLE `orderitems`
  ADD CONSTRAINT `fk_orderitems_orderid_orders_id` FOREIGN KEY (`orderid`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `fk_orderitems_pid_products_id` FOREIGN KEY (`pid`) REFERENCES `products` (`id`);

--
-- Filtros para la tabla `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `fk_orders_uid_users_id` FOREIGN KEY (`uid`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `ordertracking`
--
ALTER TABLE `ordertracking`
  ADD CONSTRAINT `fk_ordertracking_orderid_orders_id` FOREIGN KEY (`orderid`) REFERENCES `orders` (`id`);

--
-- Filtros para la tabla `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `fk_products_catid_category_id` FOREIGN KEY (`catid`) REFERENCES `category` (`id`);

--
-- Filtros para la tabla `reviews`
--
ALTER TABLE `reviews`
  ADD CONSTRAINT `fk_reviews_pid_products_id` FOREIGN KEY (`pid`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `fk_reviews_uid_users_id` FOREIGN KEY (`uid`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `usersmeta`
--
ALTER TABLE `usersmeta`
  ADD CONSTRAINT `fk_usersmeta_uid_users_id` FOREIGN KEY (`uid`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `wishlist`
--
ALTER TABLE `wishlist`
  ADD CONSTRAINT `fk_wishlist_pid_products_id` FOREIGN KEY (`pid`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `fk_wishlist_uid_users_id` FOREIGN KEY (`uid`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
